help:## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build:## build hermitcrab container
	sudo docker build . -t hermitcrab:latest

run:## jump into a shell in hermitcrab
	sudo docker run -it --rm \
		-v `pwd`:/tools \
		hermitcrab:latest \
		/bin/bash
