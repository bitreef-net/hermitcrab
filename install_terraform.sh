#!/bin/bash

TERRAFORM_VERSION="0.12.28"
TERRAFORM_ZIP="terraform_${TERRAFORM_VERSION}_linux_amd64.zip"
INSTALL_DIR="/tmp/terraform/"


mkdir -p ${INSTALL_DIR} && cd ${INSTALL_DIR}

curl -O https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/${TERRAFORM_ZIP}

unzip ${TERRAFORM_ZIP}

mv ${INSTALL_DIR}/terraform /usr/bin

rm -rf ${INSTALL_DIR}
