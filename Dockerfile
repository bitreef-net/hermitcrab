FROM debian:bullseye-20200607

WORKDIR /tools/

RUN apt update && \
	apt upgrade -y && \
	apt install vim curl unzip -y 

COPY ./install_terraform.sh /tools/

RUN ./install_terraform.sh 



