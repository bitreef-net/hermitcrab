# HermitCrab

Do one thing and do it well - Unix Philosophy

Hermitcrab stupidly breaks this principal and is a container to hold a bunch of binary tools.
It's a fancy way for me to have an ephermal desktop by having many binaries in one place.
This allows one to jump on machine and pull down a container with a bunch of necessary tools.
As great as this sounds you've been cautioned by Unix and Docker not to do this.

Let's do it anyway.


